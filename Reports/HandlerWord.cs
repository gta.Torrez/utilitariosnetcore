﻿
using Microsoft.Office.Interop.Word;
using System;

namespace Reports
{
    public class HandlerWord
    {
        public void EditDotx()
        {

            //OBJECT OF MISSING "NULL VALUE"

            Object oMissing = System.Reflection.Missing.Value;
            Object oTemplatePath = @"D:\SHARED_DOCS\plantilla.dotx";

            Application wordApp = new Application();
            Document wordDoc = new Document();
            wordDoc = wordApp.Documents.Add(ref oTemplatePath, ref oMissing, ref oMissing, ref oMissing);
            foreach (Field myMergeField in wordDoc.Fields)
            {
                Microsoft.Office.Interop.Word.Range rngFieldCode = myMergeField.Code;
                String fieldText = rngFieldCode.Text;
                // ONLY GETTING THE MAILMERGE FIELDS
                if (fieldText.StartsWith(" MERGEFIELD"))
                {
                    // THE TEXT COMES IN THE FORMAT OF
                    // MERGEFIELD  MyFieldName  \\* MERGEFORMAT
                    // THIS HAS TO BE EDITED TO GET ONLY THE FIELDNAME "MyFieldName"
                    Int32 endMerge = fieldText.IndexOf("\\");
                    Int32 fieldNameLength = fieldText.Length - endMerge;
                    String fieldName = fieldText.Substring(11, endMerge - 11);
                    // GIVES THE FIELDNAMES AS THE USER HAD ENTERED IN .dot FILE
                    fieldName = fieldName.Trim();
                    // **** FIELD REPLACEMENT IMPLEMENTATION GOES HERE ****//
                    // THE PROGRAMMER CAN HAVE HIS OWN IMPLEMENTATIONS HERE
                    if (fieldName == "Fecha")
                    {
                        myMergeField.Select();
                        wordApp.Selection.TypeText(DateTime.Now.ToString("dd/MM/yyyy"));
                    }
                    if (fieldName == "referencia")
                    {
                        myMergeField.Select();
                        wordApp.Selection.TypeText("Esta es la referencia generada para el documento");
                    }


                }

            }
            wordDoc.SaveAs(@"D:\SHARED_DOCS\plantilla.docx");
            wordApp.Documents.Open(@"D:\SHARED_DOCS\plantilla.docx");
            wordApp.Application.Quit();
        }
    }
}
