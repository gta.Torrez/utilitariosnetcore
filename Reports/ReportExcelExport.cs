﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

using NPOI.SS.Util;

namespace Reports
{
    public static class ReportExcelExport
    {

        public static MemoryStream ExportToExcelXLSX(DataTable dataToLoadExcel)
        {
            IWorkbook workbook;
            workbook = new XSSFWorkbook();
            MemoryStream memoryStream = new MemoryStream();
            ISheet excelSheet = workbook.CreateSheet("employee");
            IRow rowX = excelSheet.CreateRow(0);

            // handling header.
            foreach (DataColumn column in dataToLoadExcel.Columns)
                rowX.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

            // handling value.
            int rowIndex = 1;

            foreach (DataRow row in dataToLoadExcel.Rows)
            {
                rowX = excelSheet.CreateRow(rowIndex);

                foreach (DataColumn column in dataToLoadExcel.Columns)
                {
                    //rowX.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                    XSSFCellStyle cellStyleBlue = (XSSFCellStyle)workbook.CreateCellStyle();
                    cellStyleBlue.FillForegroundColor = IndexedColors.LightBlue.Index;
                    cellStyleBlue.FillPattern = FillPattern.SolidForeground;
                    var rowData = rowX.CreateCell(column.Ordinal);
                    rowData.CellStyle = cellStyleBlue;
                    rowData.SetCellValue(row[column].ToString());
                }

                rowIndex++;
            }

            workbook.Write(memoryStream);
            memoryStream.Flush();

            return memoryStream;
            //HttpResponse response = HttpContext.Current.Response;
            //response.ContentType = "application/vnd.ms-excel";
            //response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            //response.Clear();

            //response.BinaryWrite(memoryStream.GetBuffer());
            //response.End();
        }
        public static void ExportToExcelFileXLSX(DataTable dataToLoadExcel, string filename)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream memoryStream = new MemoryStream();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("Sheet1");
            HSSFRow headerRow = (HSSFRow)sheet.CreateRow(0);

            // handling header.
            foreach (DataColumn column in dataToLoadExcel.Columns)
                headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

            // handling value.
            int rowIndex = 1;

           foreach (DataRow row in dataToLoadExcel.Rows)
            {
                HSSFRow dataRow = (HSSFRow)sheet.CreateRow(rowIndex);

                foreach (DataColumn column in dataToLoadExcel.Columns)
                {

                    XSSFCellStyle cellStyleBlue = (XSSFCellStyle)workbook.CreateCellStyle();
                    cellStyleBlue.FillForegroundColor = IndexedColors.LightBlue.Index;
                    cellStyleBlue.FillPattern = FillPattern.SolidForeground;
                    var rowData = dataRow.CreateCell(column.Ordinal);
                        rowData.CellStyle = cellStyleBlue;
                        rowData.SetCellValue(row[column].ToString());

                }

                rowIndex++;
            }

            //workbook.Write(memoryStream);
            //memoryStream.Flush();
            using (var fileData = new FileStream(filename, FileMode.Create))
            {
                workbook.Write(fileData);
            }

            //return memoryStream;
            //HttpResponse response = HttpContext.Current.Response;
            //response.ContentType = "application/vnd.ms-excel";
            //response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            //response.Clear();

            //response.BinaryWrite(memoryStream.GetBuffer());
            //response.End();
        }

        public static MemoryStream ExportToExcelXLS(DataTable dataToLoadExcel)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream memoryStream = new MemoryStream();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("Sheet1");
            HSSFRow headerRow = (HSSFRow)sheet.CreateRow(0);

            // handling header.
            foreach (DataColumn column in dataToLoadExcel.Columns)
                headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

            // handling value.
            int rowIndex = 1;

            foreach (DataRow row in dataToLoadExcel.Rows)
            {
                HSSFRow dataRow = (HSSFRow)sheet.CreateRow(rowIndex);

                foreach (DataColumn column in dataToLoadExcel.Columns)
                {
                    dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                }

                rowIndex++;
            }

            workbook.Write(memoryStream);
            memoryStream.Flush();

            return memoryStream;
            //HttpResponse response = HttpContext.Current.Response;
            //response.ContentType = "application/vnd.ms-excel";
            //response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            //response.Clear();

            //response.BinaryWrite(memoryStream.GetBuffer());
            //response.End();
        }
        public static void ExportToExcelFileXLS(DataTable dataToLoadExcel, string filename)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream memoryStream = new MemoryStream();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("Sheet1");
            HSSFRow headerRow = (HSSFRow)sheet.CreateRow(0);

            // handling header.
            foreach (DataColumn column in dataToLoadExcel.Columns)
                headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

            // handling value.
            int rowIndex = 1;

            foreach (DataRow row in dataToLoadExcel.Rows)
            {
                HSSFRow dataRow = (HSSFRow)sheet.CreateRow(rowIndex);

                foreach (DataColumn column in dataToLoadExcel.Columns)
                {
                    dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                }

                rowIndex++;
            }

            //workbook.Write(memoryStream);
            //memoryStream.Flush();
            using (var fileData = new FileStream(filename, FileMode.Create))
            {
                workbook.Write(fileData);
            }

            //return memoryStream;
            //HttpResponse response = HttpContext.Current.Response;
            //response.ContentType = "application/vnd.ms-excel";
            //response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            //response.Clear();

            //response.BinaryWrite(memoryStream.GetBuffer());
            //response.End();
        }
    }
}
