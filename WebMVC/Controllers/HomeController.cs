﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebMVC.Models;
using Wkhtmltopdf.NetCore;

namespace WebMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly IGeneratePdf generatePdf;
        public HomeController(ILogger<HomeController> logger, IGeneratePdf generatePdf)
        {
            this.generatePdf = generatePdf;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        } 
        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> pdfData()
        {
            List<Person> people = new List<Person>() {
            new Person(){Name="Juan"},
            new Person(){Birthday="29/01/1993"},
            new Person(){Birthday="29/01/1993",Name="Luis",Age="12"},
            new Person(){Birthday="29/01/1995"},
            };
            var a = await generatePdf.GetPdf("Views/Home/Table/_Person.cshtml", people);
            return a;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
