﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.Models
{
    public class Person
    {
        public string Name { get; set; }
        public string Birthday { get; set; }
        public string Age { get; set; }
    }
}
