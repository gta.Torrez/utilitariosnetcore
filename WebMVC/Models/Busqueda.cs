﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.Models
{
    public class Busqueda
    {
        [StringLength(250)]
        public string NombreCompleto { get; set; }

        [StringLength(24)]
        public string NumeroTramite { get; set; }

        [StringLength(20)]
        [Range(0, Int64.MaxValue, ErrorMessage = "Este campo debe ser solo números")]
        [Number(ErrorMessage = "Este campo debe ser solo números")]
        public string NITCI { get; set; }
        [DataType(DataType.Date)]
        public DateTime? FechaInicial { get; set; }
        [DataType(DataType.Date)]
        public DateTime? FechaFinal { get; set; }
    }
    public class Number : RegularExpressionAttribute
    {
        public Number()
            : base("^[0-9]*$")
        {
        }
    }
}
